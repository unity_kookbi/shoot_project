﻿using Core;
using UnityEngine;

public static class VectorExtensions
{
    public static Vector3D ToVector3D(this Vector3 vector)
        => new Vector3D(vector.x, vector.y, vector.z);

    public static Vector3 ToVector3(this Vector3D vector)
        => new Vector3(vector.x, vector.y, vector.z);

}

