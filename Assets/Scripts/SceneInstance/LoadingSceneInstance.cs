using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public sealed class LoadingSceneInstance : SceneInstanceBase
{
    [Header("# 로딩 상태바")]
    public Image m_LoadingProgressFill;

    [Header("# 로딩 상태바 채우기 속도")]
    public float m_ProgressFillSpeed = 2.0f;

    /// <summary>
    /// 실제 로드 상태가 아닌 표시용 수치를 나타납내다.
    /// </summary>
    private float _TargertProgressValue;

    protected override void Awake()
    {
        base.Awake();

        m_LoadingProgressFill.fillAmount = 0.0f;
    }

    private void Start() => StartCoroutine(StartLoading());

    private void Update() => UpdateProgressFill(_TargertProgressValue);

    private IEnumerator StartLoading()
    {
        // 로드시킬 씬 이름
        string targetSceneName = SceneManager.instance.nextSceneName;

        // 비동기적을 씬을 로드합니다.
        AsyncOperation asyncOperation = UnitySceneManager.LoadSceneAsync(targetSceneName);

        // 로드 작업이 끝나도, 즉각적인 씬 전환을 허용하지 않습니다.
        asyncOperation.allowSceneActivation = false;
        // 씬 전환 이외의 로드 작업이 모두 끝나도 progress 는 90%(0.9f) 까지만 
        // 증가 됩니다.

        while (asyncOperation.progress < 0.9f)
        {
            // 90% 이상인 경우 100% 로 표시되도록 합니다.
            _TargertProgressValue = asyncOperation.progress;
            yield return null;
        }

        // 90% 이상인 경우 100&로 표시되도록합니다.
        _TargertProgressValue = 1.0f;

        // 상태바가 거의 찰때까지 대기합니다
        yield return new WaitUntil(() => m_LoadingProgressFill.fillAmount > 0.999f);

        // 1초 대기합니다.
        yield return new WaitForSecondsRealtime(1.0f);

        // 로드시킨 씬으로의  전환을 허용합니다.
        asyncOperation.allowSceneActivation = true;
    }

    private void UpdateProgressFill(float progress)
    {
        float currentFill = m_LoadingProgressFill.fillAmount;
        currentFill = Mathf.MoveTowards(
            currentFill,
            progress,
            m_ProgressFillSpeed * Time.deltaTime);

        m_LoadingProgressFill.fillAmount = currentFill;
    }

}
