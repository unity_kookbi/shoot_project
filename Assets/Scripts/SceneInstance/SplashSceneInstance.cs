using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 스플래시 씬 객체를 나타내기 위한 클래스입니다.
/// </summary>
public sealed class SplashSceneInstance : SceneInstanceBase
{
    [SerializeField]
    private float _SplashShowTimeSeconds = 2.0f;

    protected override void Awake()
    {
        base.Awake();

        StartCoroutine(StartSplashShowDelay());

    }

    private IEnumerator StartSplashShowDelay()
    {
        // 2초간 대기합니다.
        yield return new WaitForSecondsRealtime(_SplashShowTimeSeconds);

        // 로그인 씬으로 전환합니다.
        (SceneManager.instance as SceneManager).LoadScene(Game.Constants.SCENENAME_LOGIN);
    }
}
