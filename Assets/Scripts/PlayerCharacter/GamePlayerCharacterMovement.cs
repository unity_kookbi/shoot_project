using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GamePlayerCharacterMovement : MonoBehaviour
{
    [Header("# 이동 관련")]
    [SerializeField]
    private float _MaxMoveSpeed;

    private GamePlayerCharacter _Owner;

    private CharacterController _CharacterController;

    private Vector3 _Direction;

    /// <summary>
    /// 목표 위치 (내가 조종하는 캐릭터가 아닌 경우 사용됩니다.)
    /// </summary>
    private Vector3 _TargetPosition;

    /// <summary>
    /// 목표 앞 방향 (내가 조종하는 캐릭터가 아닌 경우 사용됩니다.)
    /// </summary>
    private Vector3 _TargetForwardDirection;

    private void Awake()
    {
        _Owner = GetComponent<GamePlayerCharacter>();
        _CharacterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        // 내가 조종하는 캐릭터라면
        if(_Owner.isLocal)
        {
            _CharacterController.SimpleMove(_Direction * _MaxMoveSpeed);
        }
        // 내가 조종하는 캐릭터가 아니라면
        else
        {
            UpdateTargetPositionAndDirection();         
        }
    }

    public void SetForwardDirection(Vector3 forwardDirection)
    {
        transform.forward = forwardDirection;
    }

    public void OnMoveInput(Vector2 inputAxis)
    {
        Vector3 moveDirection = new Vector3(inputAxis.x, 0.0f, inputAxis.y);
        moveDirection.Normalize();
        _Direction = moveDirection;
    }

    /// <summary>
    /// 서버에서 받은 데이터로 갱신시킵니다.
    /// </summary>
    public void UpdateTargetPositionAndDirection()
    {
        Vector3 nextPosition = Vector3.MoveTowards(
            transform.position, _TargetPosition, _MaxMoveSpeed * Time.deltaTime);

        float targetYawAngle = Mathf.Atan2(_TargetForwardDirection.z, _TargetForwardDirection.x) * Mathf.Rad2Deg;

        Quaternion targetRotation = Quaternion.Euler(0.0f, targetYawAngle, 0.0f);
        Quaternion currentRotation = transform.rotation;
        Quaternion nextRotation = Quaternion.Slerp(currentRotation, targetRotation, _MaxMoveSpeed * 10 * Time.deltaTime);

        transform.position = nextPosition;
        transform.rotation = nextRotation;
    }

    /// <summary>
    /// 목표 위치와 목표 방향을 설정합니다.
    /// </summary>
    /// <param name="targetPosition">목표 위치를 전달합니다.</param>
    /// <param name="targetDirection">목표 방향을 전달합니다.</param>
    public void SetTargetPositionAndDirection(Vector3 targetPosition, Vector3 targetDirection)
    {
        _TargetPosition = targetPosition;
        _TargetForwardDirection = targetDirection;
    }
}
