using Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor.Search;
using UnityEngine;

/// <summary>
/// 서버와 통신을 위한 기능을 제공하는 네트워크 매니저
/// </summary>
public sealed class NetworkManager : ManagerClassBase<NetworkManager>
{
    /// <summary>
    /// 연결된 서버 소켓
    /// </summary>
    public Socket connectedServerSocket { get; private set; }

    /// <summary>
    /// 서버 연결을 위한 쓰레드
    /// </summary>
    private Thread _ConnectThread;

    /// <summary>
    /// 서버에 대한 응답을 받기위한 쓰레드
    /// </summary>
    private Thread _ListenThread;

    /// <summary>
    /// 서버 재연결 최대 시도 횟수
    /// </summary>
    private int _MaxTryCount = 5;

    /// <summary>
    /// 패킷 수신 이벤트
    /// </summary>
    private Dictionary<PacketType, Action<PacketType, object>> _PacketReceivedEvent = new();

    /// <summary>
    /// 마지막으로 서버와 소통한 시간을 기록합니다.
    /// </summary>
    private float _LastCommunicatedTime;

    public string id { get; private set; }
    public string nickname { get; private set; }

    private void OnApplicationQuit()
    {
        try
        {
            if(connectedServerSocket.Connected)
            {
                SendPacket<SimpleResponsePacket>(PacketType.IsDisconnected, new(true));
            }

            if(_ConnectThread.ThreadState != ThreadState.Unstarted)
                _ConnectThread.Abort();


            if(_ListenThread.ThreadState != ThreadState.Unstarted)
                _ListenThread.Abort();
        }
        catch(ThreadAbortException) { }
    }

    public override void OnManagerInitialized()
    {
        base.OnManagerInitialized();

        // 서버에 연결
        _ConnectThread = new Thread(Connect);
        _ConnectThread.Start();
    }

    public void Connect()
    {
        int tryCount = 0;
        do
        {
            // 일단 먼저 접속을 시도합니다.
            ConnectedToServer();            
            Thread.Sleep(2000);
            ++tryCount;
        }
        // 접속에 실패한다면 다시 접속을 시도합니다.
        while (!connectedServerSocket.Connected && tryCount < _MaxTryCount);


        // 서버에서 데이터를 받도록 합니다.
        _ListenThread = new Thread(DoListen);
        _ListenThread.Start();
    }

    /// <summary>
    /// 서버에 연결시킵니다.
    /// </summary>
    private void ConnectedToServer()
    {

        IPEndPoint endPoint = new(IPAddress.Parse(Constants.ADDRESS),
            Constants.SERVERPORT);

        connectedServerSocket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        try 
        { 
            connectedServerSocket.Connect(endPoint);
        }
        catch (SocketException)
        {
            Debug.Log("연결에 실패하였습니다.");
            Debug.Log("연결을 재시도합니다.");
        }

    }

    private async void DoListen()
    {
        while (true) 
        {
            // 패킷을 받습니다.
            PacketType packetType = await Packet.ReceiveAsyncPacketType(connectedServerSocket);

            // 전달된 데이터를 저장하기 위한 변수
            object data = null;

            // 전달된 패킷 타입에 따라 분기합니다.
            switch(packetType)
            {
                case PacketType.IsDisconnected:

                    break;

                case PacketType.CreateAccountResponse:
                    data = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(connectedServerSocket);
                    break;

                case PacketType.LoginResponse:
                    LoginResponsePacket loginResponsePacket = await Packet.ReceiveAsyncHeaderAndData<LoginResponsePacket>(connectedServerSocket);
                    data = loginResponsePacket;
                    
                    // 로그인 성공 시, id / nickname 을 기록합니다.
                    if(loginResponsePacket.result)
                    {
                        this.id = loginResponsePacket.id;
                        this.nickname = loginResponsePacket.nickname;
                    }
                    break;

                case PacketType.ConnectToWorldResponse:
                    data = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(connectedServerSocket);

                    Dispatcher.Enqueue(() => StartCoroutine(ClientTick()));
                    break;

                case PacketType.PlayerListResponse:
                    data = await Packet.ReceiveAsyncHeaderAndData<PlayerListResponsePacket>(connectedServerSocket);
                    break;

                case PacketType.NewPlayerConnected:
                    data = await Packet.ReceiveAsyncHeaderAndData<NewPlayerConnectionPacket>(connectedServerSocket);
                    break;

                case PacketType.PlayerDisconnected:
                    data = await Packet.ReceiveAsyncHeaderAndData<PlayerDisconnectedPacket>(connectedServerSocket);
                    break;

                case PacketType.UpdatedPlayerPositionResponse:
                    data = await Packet.ReceiveAsyncHeaderAndData<UpdatedPlayerPositionsPacket>(connectedServerSocket);
                    break;
            }

            // 전달된 패킷 타입에 따라 이벤트를 발생시킵니다.
            _PacketReceivedEvent[packetType]?.Invoke(packetType, data);
        }
    }

    private IEnumerator ClientTick()
    {
        WaitForSecondsRealtime wait01 = new WaitForSecondsRealtime(0.1f);

        while(true)
        {
            yield return wait01;

            // 마지막으로 서버와 소통한 시간이 0.5초를 넘기는 경우 서버에게 패킷을 보냅니다.
            if(_LastCommunicatedTime + 0.5f < Time.time)
            {
                SendPacket<SimpleRequestPacket>(PacketType.UpdateOnlineStateRequest, new());
            }
        }
    }

    public void SendPacket<T>(PacketType packetType, T data) where T : struct
    {
       Task.Factory.StartNew(async () =>
            await Packet.SendAsync<T>(connectedServerSocket, packetType, data));
    }

    public void AddPacketEvent(PacketType packetType ,Action<PacketType, object> packetEvent)
    {
        if(_PacketReceivedEvent.ContainsKey(packetType))
            _PacketReceivedEvent[packetType] += packetEvent;
        else _PacketReceivedEvent.Add(packetType, packetEvent);
    }

    public void RemovePacketEvent(PacketType packetType, Action<PacketType, object> removeEvent)
    {
        if(_PacketReceivedEvent.ContainsKey(packetType))
            _PacketReceivedEvent[packetType] -= removeEvent;
    }

}
