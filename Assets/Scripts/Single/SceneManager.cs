using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public sealed class SceneManager : SceneManagerBase
{
    public static SceneManager _ThisInstance;

    public static new SceneManager instance => _ThisInstance ?? (_ThisInstance = GameManagerBase.Get().GetManager<SceneManager>());

    /// <summary>
    /// 다음으로 로드시킬 씬 이름을 나타냅니다.
    /// </summary>
    public string nextSceneName {  get; private set; }

    /// <summary>
    /// 씬을 로딩씬을 거쳐 비동기적으로 로드합니다.
    /// </summary>
    /// <param name="nextSceneName">전환시킬 씬 이름을 전달합니다.</param>
    public void LoadScene(string nextSceneName)
    {
        // 다음 씬 이름 설정
        this.nextSceneName = nextSceneName;

        // 일단 LoadingScene 으로 전환합니다.
        UnitySceneManager.LoadScene(Game.Constants.SCENENAME_LOADING);
    }
}
