using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NicknameHUD : MonoBehaviour
{
    [Header("# 닉네임 텍스트")]
    [SerializeField]
    private TMP_Text _NicknameText;

    public RectTransform rectTransform => transform as RectTransform;

    public void SetNickname(string userNickname)
    {
        _NicknameText.text = userNickname;
    }

    /// <summary>
    /// HUD 위치를 갱신합니다.
    /// </summary>
    /// <param name="characterPosition"></param>
    public void UpdateHUDPosition(Vector3 characterPosition)
    {
        Vector3 objectWorldPosition = characterPosition + (Vector3.up * 2.0f);

        Vector3 viewportPosition = Camera.main.WorldToScreenPoint(objectWorldPosition);

        viewportPosition.z = 0.0f;

        rectTransform.anchoredPosition = viewportPosition;
    }
}
