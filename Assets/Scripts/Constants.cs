﻿
namespace Game
{
    public static class Constants
    {
        /// <summary>
        /// 로그인 씬 이름
        /// </summary>
        public const string SCENENAME_LOGIN = "LoginScene";

        /// <summary>
        /// 로딩 씬 이름
        /// </summary>
        public const string SCENENAME_LOADING = "LoadingScene";

        /// <summary>
        /// 게임 씬 이름
        /// </summary>
        public const string SCENENAME_GAME = "GameScene";
    }
}
